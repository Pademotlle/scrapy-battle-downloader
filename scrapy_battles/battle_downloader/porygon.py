import asyncio
import time
from datetime import datetime
from shutil import which
import re

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from scrapy_battles import constants


class Download:
    """
    Consumer that gets the event to retrieve URL battles
    """

    def __init__(self):
        """
        Class constructor
        """

        self.battle_urls = asyncio.Queue(maxsize=10000)
        self.battle_id_regex = re.compile(pattern=constants.BATTLE_ID_REGEX)

    @staticmethod
    def _load_driver_settings() -> webdriver.Chrome:
        """
        This method just loads the selenium Chrome Driver settings before usage
        """
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        preferences = {"download.default_directory": constants.DEFAULT_DOWNLOAD_DIR}
        chrome_options.add_experimental_option("prefs", preferences)
        chrome_path = which("chromedriver")
        return webdriver.Chrome(executable_path=chrome_path, options=chrome_options)

    @staticmethod
    def _open_pokemon_battle(web_driver: webdriver.Chrome, battle_url: str):
        """
        This method uses selenium to open pokemon showdown web page

        Args:
            web_driver: Web Driver for Google Chrome
            battle_url: Retrieved Pokemon battle link from PokemonShowdown
        """

        print(f"Opening Pokemon battle: {battle_url}")
        web_driver.get(battle_url)

    @staticmethod
    def _enable_download_in_headless_chrome(web_driver):
        # add missing support for chrome "send_command"  to selenium webdriver
        web_driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')

        params = {
            'cmd': 'Page.setDownloadBehavior',
            'params': {
                'behavior': 'allow',
                'downloadPath': constants.DEFAULT_DOWNLOAD_DIR
            }
        }
        web_driver.execute("send_command", params)

    @staticmethod
    def _close_selenium_web_driver(web_driver: webdriver.Chrome):
        """
        This method closes the Selenium Web driver

        Args:
            web_driver: Web Driver for Google Chrome
        """

        web_driver.close()

    def download_battle_log_file(self, battle_url: str):
        """
        This method selects the Refresh button in order to update the current Pokemon battles
        """

        download_button = None
        driver = self._load_driver_settings()
        self._open_pokemon_battle(web_driver=driver, battle_url=battle_url)
        self._enable_download_in_headless_chrome(web_driver=driver)
        init_time = datetime.now()

        while not download_button and (datetime.now() - init_time).seconds < constants.WAITING_TIME_LIMIT:

            try:
                download_button = driver.find_element_by_css_selector(constants.DOWNLOAD_BUTTON_CCS)
                time.sleep(1)
                download_button.click()
                print(f"Downloading file for battle {self.battle_id_regex.match(battle_url).group('battle_id')}")

            except NoSuchElementException:
                time.sleep(4)
        time.sleep(4)
        self._close_selenium_web_driver(web_driver=driver)
