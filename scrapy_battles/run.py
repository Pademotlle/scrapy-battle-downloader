import asyncio
import threading

from scrapy_battles.battle_downloader import porygon
from scrapy_battles.rabbitmq_consumer import bunnelby
from scrapy_battles.battle_parser import perrserker


class DownloadException(Exception):
    """
    Base Exception
    """


class BattleDownloader:
    """
    This class scraps the battle logs from PokemonShowdown battle web links
    """

    def __init__(self):
        """
        Class Constructor
        """

        self.consumer = bunnelby.RabbitPickup()
        self.downloader = porygon.Download()

    async def connect_to_broker(self, event_loop):
        """
        This method established connection with the RabbitMQmessage broker
        """

        await self.consumer.connect_to_msg_broker(event_loop=event_loop)

    async def start_consuming(self):
        """
        This method makes the RabbitMQ client to start consuming
        """

        await self.consumer.consume()

    async def download_battle_logs(self):
        """
        This method downloads the battle log from a Pokemon battle
        """

        while True:

            try:
                battle_url = await self.consumer.get_battle_url
                download_thread = threading.Thread(target=self.downloader.download_battle_log_file, args=(battle_url, ))
                download_thread.start()

            except IndexError:
                pass


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    battle_downloader = BattleDownloader()

    try:
        future_tasks = asyncio.gather(
            battle_downloader.connect_to_broker(event_loop=loop),
            battle_downloader.start_consuming(),
            battle_downloader.download_battle_logs()
        )
        loop.run_until_complete(future_tasks)

    except DownloadException:
        battle_downloader.consumer.close_broker_connection()
