import json

import aio_pika
import asyncio

from scrapy_battles import constants


class RabbitPickup:
    """
    Consumer that gets the battle URLs from queue
    """

    def __init__(self):
        """
        Class constructor
        """

        self.battle_urls = asyncio.Queue(maxsize=10000)
        self.rabbitmq_connection = None
        self.pika_channel = None
        self.queue = None

    @property
    async def get_battle_url(self):
        battle_format_and_url = json.loads(await self.battle_urls.get())
        return list(battle_format_and_url.values()).pop()

    async def connect_to_msg_broker(self, event_loop):
        """
        This method establishes connection and creates a channel to the RabbitMQ message broker
        """

        print("Connecting to RabbitMQ...")
        self.rabbitmq_connection = await aio_pika.connect_robust(
            constants.RABBITMQ_URL,
            loop=event_loop
        )
        self.pika_channel = await self.rabbitmq_connection.channel()
        self.queue = await self.pika_channel.get_queue(constants.URLS_QUEUE)

    async def _on_message_reception(self, message: aio_pika.IncomingMessage):
        """
        Callback method to be launched when a message from RabbitMQ queue is consumed
        """

        print(f" [x] Url received")
        await self.battle_urls.put(message.body.decode("utf-8"))

    async def consume(self):
        """
        This method listens continually if there are new battle URLs in queue
        """

        print("Start consuming")
        await asyncio.sleep(0.5)
        await self.queue.consume(self._on_message_reception, no_ack=True)

    def close_broker_connection(self):
        """
        This method closes the connection with RabbitMQ
        """

        print("Closing RabbitMQ connection...")
        self.rabbitmq_connection.close()
        print("RabbitMQ connection closed!")