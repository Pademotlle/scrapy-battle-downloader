import os

RABBITMQ_URL = "amqp://xavi:handball@127.0.0.1/"
URLS_QUEUE = "urls"
PLAY_POKEMON_SHOWDOWN_URL = "https://play.pokemonshowdown.com/"
DOWNLOAD_BUTTON_CCS = "a.button.replayDownloadButton"
DEFAULT_DOWNLOAD_DIR = os.path.abspath(os.path.join(os.curdir, "battle_logs"))
WAITING_TIME_LIMIT = 400
BATTLE_ID_REGEX = r".*-(?P<battle_id>\d+)"